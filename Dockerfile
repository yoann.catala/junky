FROM node:8.6.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
RUN mkdir node_modules
VOLUME /node_modules
ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
COPY package.json /usr/src/app/
RUN ls
RUN npm install && npm cache clean --force
COPY . /usr/src/app

RUN ls node_modules
CMD [ "npm", "start" ]